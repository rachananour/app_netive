import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { MainStackNavigator,CategoryStackNavigator, ContactStackNavigator } from './StackNavigator'
import {AdjustmentsIcon, HomeIcon } from 'react-native-heroicons/outline'
const Tab = createBottomTabNavigator();

const BottomTabNavigator = () => {
  return (
    <Tab.Navigator
      screenOptions={{
        // swipeEnabled: false,
        headerShown: false,
        tabBarShowLabel: true,
        tabBarStyle: {
          backgroundColor: '#AD40AF' 
        },
        // tabBarScrollEnabled: true,
        tabBarActiveTintColor: "#fff",
        // tabBarInactiveTintColor: "yellow",
    }}
    >
      <Tab.Screen 
        name="Profile" 
        component={MainStackNavigator}  
        options={{
          tabBarIcon: ({color, size, focused}) => (
            <HomeIcon size={20} color="#fff" />
          ),
        }}
      />
      <Tab.Screen 
        name="Category" 
        component={CategoryStackNavigator}  
        options={{
          tabBarIcon: ({color, size, focused}) => (
            <AdjustmentsIcon size={20} color="#fff" />
          ),
        }}
      />
    
      <Tab.Screen 
        name="Contact" 
        component={ContactStackNavigator} 
        options={{
          tabBarIcon: ({color, size, focused}) => (
            <HomeIcon size={20} color="#fff" />
          ),
        }}
      />
    </Tab.Navigator>
  );
}

export default BottomTabNavigator