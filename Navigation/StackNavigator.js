import React from "react";
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import Home from "../Screens/Home";
import Category from "../Screens/Category";
import About from "../Screens/About";
import Contact from "../Screens/Contact";

const Stack = createNativeStackNavigator();

const screenOptionStyle = {
  headerStyle: {
    backgroundColor: "#9AC4F8",
  },
  headerTintColor: "red",
  headerBackTitle: "Back",
};

const MainStackNavigator = (navigation) => {
  return (
    <Stack.Navigator screenOptions={{screenOptionStyle}}>
      <Stack.Screen 
        navigation={navigation}
        name="PageHome" 
        component={Home} 
        options={{headerShown:false, headerTitle:"Back"}}
        
      />
      <Stack.Screen 
        navigation={navigation}
        name="About" 
        component={About}
      />
    </Stack.Navigator>
  );
};

const ContactStackNavigator = (navigation) => {
  return (
    <Stack.Navigator screenOptions={{screenOptionStyle, headerShown: false}}>
      <Stack.Screen 
        navigation={navigation}
        name="Page Contact" 
        component={Contact} 
      />
    </Stack.Navigator>
  );
};

const CategoryStackNavigator = (navigation) => {
  return (
    <Stack.Navigator screenOptions={{screenOptionStyle, headerShown: false}}>
      <Stack.Screen 
        navigation={navigation}
        name="Page Category" 
        component={Category} 
      />
    </Stack.Navigator>
  );
};

export { MainStackNavigator, CategoryStackNavigator, ContactStackNavigator};