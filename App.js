import 'react-native-gesture-handler';
// Import React and Component
import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';


import BottomTabNavigator from "./Navigation/TabNavigator";
import DrawerNavigator from './Navigation/DrawerNavigator';

// import { MainStackNavigator } from "./Navigation/StackNavigator";

const Stack = createNativeStackNavigator();

const App = () => {
  return (
    <NavigationContainer>
     <Stack.Navigator initialRouteName="HomePage">
          <Stack.Screen
            name="HomePage"
            component={DrawerNavigator}
            options={{headerShown: false}}
          />
        </Stack.Navigator>
    </NavigationContainer>
  )
}
export default App;