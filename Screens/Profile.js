import { View, StyleSheet, Image, SafeAreaView, Text } from 'react-native'
import React, { useState } from "react";

export default function Profile({ navigation }) {
  return (
    <View style={styles.container}>
      <View style={styles.profileUserStyle}>
        <Image
          style={styles.profileUser}
          source={{
            uri: 'https://reactnative.dev/img/tiny_logo.png',
          }}
        />
      </View>
      <View>
        <Text>
          <Text style={styles.titleText}>Nour Rachana</Text>
        </Text>
        <Text>
          <Text style={styles.baseText}>Nour Rachana</Text>
        </Text>
      </View>
    </View>

  );
}

const styles = StyleSheet.create({
  container: {
    paddingTop: 50,
  },
  profileUserStyle: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  profileUser: {
    width: 60,
    height: 60,
  },
  titleText: {
    fontSize: 20,
    fontWeight: "bold",
    paddingTop: 50,
  },
  baseText: {
    textAlign: 'right',
    fontFamily: "Cochin",
  },
});
